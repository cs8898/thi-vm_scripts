#--------------------------------
# Some more ls aliases
#
alias ll='ls -la'
alias la='ls -lah'
#alias l='ls -aF'

#--------------------------------
# Ad LaTeX

alias clean='rm -f *~ *bak'
alias cleantex='rm -f *.aux *.log *.out *.toc'

#--------------------------------
# Recode
# See tutorial in 'info recode'
# Maybe its better to use
#    cat files | recode pc > file.new
# Overwrite-mode 'recode pc file' i snot idempotent

alias dos2unix='recode ibmpc..lat1'
alias unix2dos='recode lat1..ibmpc'

#--------------------------------
### hg external diff tool
alias hgextdiff='hg extdiff -p kdiff3'

#--------------------------------
# Starwars als ASCII-Art
alias starwars='telnet towel.blinkenlights.nl 23'

