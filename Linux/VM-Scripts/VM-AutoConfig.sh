#!/bin/bash
# Auto configuration of VM in Kiosk-Mode.
# Run all scripts in directory /media/sf_H_DRIVE/Linux/VM-Scripts/DoByLogin

if [[ -d /media/sf_H_SHARE/Linux/VM-Scripts/DoByLogin ]]
then
  for f in `ls /media/sf_H_SHARE/Linux/VM-Scripts/DoByLogin/*.sh`
  do
    scriptname=`basename $f`
    echo "Executing $scriptname"
    /bin/bash $f | tee /tmp/DoByLogin_${USER}.${scriptname}.out 2>&1
  done
fi

