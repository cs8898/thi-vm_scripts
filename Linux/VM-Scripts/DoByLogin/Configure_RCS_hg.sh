#!/bin/bash

echo "#---------------------------"
echo "# Auto configuration of hg"
echo "#---------------------------"
echo ""

# Copy file .hgrc

if [ -e /media/sf_H_SHARE/Linux/VM-Config/hg/dot.hgrc ]
then
  cp /media/sf_H_SHARE/Linux/VM-Config/hg/dot.hgrc $HOME/.hgrc
fi

