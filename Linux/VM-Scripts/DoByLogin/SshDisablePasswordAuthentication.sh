#!/bin/bash
#
# Hardening of sshd config

if [ -f /etc/ssh/sshd_config ]; then
  egrep -v '^PasswordAuthentication' /etc/ssh/sshd_config > /tmp/sshd_config
  echo 'PasswordAuthentication no' >> /tmp/sshd_config
  sudo cp /tmp/sshd_config /etc/ssh/sshd_config
  sudo chmod 0644 /etc/ssh/sshd_config
  sudo service ssh restart
fi

