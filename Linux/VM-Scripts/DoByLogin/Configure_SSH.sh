#!/bin/bash

# Create local SSH configuration
echo "#---------------------------"
echo "# Auto configuration of SSH"
echo "#---------------------------"
echo ""

mkdir -p -m 0700 $HOME/.ssh

if [ -d /media/sf_H_SHARE/Linux/VM-Config/ssh ]
then
  # Update files, do not delete already existing files
  cp /media/sf_H_SHARE/Linux/VM-Config/ssh/* $HOME/.ssh
fi

# Set permissions

# File authorized_keys
if [ -f $HOME/.ssh/authorized_keys ]
then
  chmod 0755 $HOME/.ssh/authorized_keys
fi

# File config
if [ -f $HOME/.ssh/config ]
then
  chmod 0755 $HOME/.ssh/config
fi

# Private keys
for f in $(ls $HOME/.ssh/id_rsa* | grep -v pub)
do
  chmod 0600 $f
done

# Public keys
for f in $(ls $HOME/.ssh/id_rsa* | grep pub)
do
  chmod 0644 $f
done

