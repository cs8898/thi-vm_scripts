#!/bin/bash

# Install zsh And chshell
echo "#---------------------------"
echo "#   Install i3wm and Tools  "
echo "#---------------------------"
echo ""

echo "lars+glas" | sudo -S whoami
sudo apt update -y
sudo apt install -y zsh
sudo chsh -s /bin/zsh lars

echo "# Setting .zshrc"
wget -O ~/.zshrc https://raw.githubusercontent.com/grml/grml-etc-core/master/etc/zsh/zshrc 
