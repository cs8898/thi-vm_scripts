#!/bin/bash

# Add Google To Firefox
echo "#--------------------------"
echo "#   Add Google To Firefox  "
echo "#--------------------------"
echo ""

cat << EOF > /tmp/ffaddgoogle.html
<html>
  <head>
    <script>
        window.external.AddSearchProvider('https://linuxmint.com/searchengines/google.xml');
    </script>
  </head>
</html>

EOF

firefox file:///tmp/ffaddgoogle.html
