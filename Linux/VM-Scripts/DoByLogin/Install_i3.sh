#!/bin/bash

# Install i3 And Tools
echo "#---------------------------"
echo "#   Install i3wm and Tools  "
echo "#---------------------------"
echo ""

echo "lars+glas" | sudo -S whoami
sudo apt update -y
sudo apt install -y i3 suckless-tools i3status
