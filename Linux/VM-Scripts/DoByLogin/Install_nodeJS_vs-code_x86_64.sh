#!/bin/bash

# Install vscode and node
echo "#----------------------------"
echo "#  Install VSCode and NodeJS "
echo "#       Only x86_64          "
echo "#----------------------------"
echo ""

if [ $(lscpu | grep x86_64 | wc -l) -le 0 ]; then
  echo "This is not 64Bit, will exit now"
  exit 0
fi


echo "lars+glas" | sudo -S whoami

# Install NodeLTS
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs

# Install Yarn
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
     echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
     sudo apt-get update -y && sudo apt-get install -y yarn

# Add Repo
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/packages.microsoft.gpg
sudo install -o root -g root -m 644 /tmp/packages.microsoft.gpg /usr/share/keyrings/
sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

#Update and install
sudo apt install -y apt-transport-https
sudo apt update -y
sudo apt install -y code

