#!/bin/bash

# Git Clone PGP1
echo "#---------------------------"
echo "# Clone PGP from Bitbucket  "
echo "#---------------------------"
echo ""
 
repos=(git@bitbucket.org:cs8898/pgp1.git git@bitbucket.org:cs8898/rap.git)
mkdir ~/Studium
cd ~/Studium

for i in "${repos[@]}"
do
  git clone $i
done
