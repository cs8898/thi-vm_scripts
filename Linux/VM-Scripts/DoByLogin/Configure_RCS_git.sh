#!/bin/bash

echo "#---------------------------"
echo "# Auto configuration of git"
echo "#---------------------------"
echo ""

# Copy file .gitconfig

if [ -e /media/sf_H_SHARE/Linux/VM-Config/git/dot.gitconfig ]
then
  cp /media/sf_H_SHARE/Linux/VM-Config/git/dot.gitconfig $HOME/.gitconfig
fi

