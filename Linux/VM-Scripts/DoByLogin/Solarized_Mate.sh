echo "#-----------------"
echo "# Solarized"
echo "#-----------------"

mkdir /tmp/solarized-git
cd /tmp/solarized-git

git clone https://github.com/oz123/solarized-mate-terminal.git
cd solarized-mate-terminal
bash solarized-mate.sh

dconf dump /org/mate/terminal/profiles/solarized-dark/ > /tmp/mate-term-solarized.dconf
dconf load /org/mate/terminal/profiles/default/ < /tmp/mate-term-solarized.dconf
