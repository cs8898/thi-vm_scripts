#!/bin/bash

echo "#---------------------------"
echo "# Auto configuration of bash"
echo "#---------------------------"
echo ""

# Copy file .bash_aliases

if [ -e /media/sf_H_SHARE/Linux/VM-Config/rcfiles/dot.bash_aliases ]
then
  cp /media/sf_H_SHARE/Linux/VM-Config/rcfiles/dot.bash_aliases $HOME/.bash_aliases
fi

