# THI Vm Skripts


## TL;DR

1. copy to `HomeShare`
2. copy ssh-key to `./VM-Config/ssh/`
3. modify `./VM-Config/git/dot.gitconfig` to match your repository and email
4. delete unwanted scripts in `./VM-Scripts/DoByLogin/` (eg `Solarized_Mate.sh`, `Install_[i3|zsh].sh`)
5. modify `./VM-Scripts/DoByLogin/ZZ_Git_Clone.sh` to match your needs

6. Manually run the `./VM-Scripts/VM-AutoConfig.sh` on each boot

## SOME STUFF

The Scripts try to help you setting up the VM to your needs...
There are Scripts for installing i3 and zsh
Also there is a little helper to activate vim linenumbers (`ZZ_VIMRC.sh`)


## Add SSH Key

move to `./VM-Config/ssh/` and run `ssh-keygen`  
enter `./id_rsa` when asked for the file path
